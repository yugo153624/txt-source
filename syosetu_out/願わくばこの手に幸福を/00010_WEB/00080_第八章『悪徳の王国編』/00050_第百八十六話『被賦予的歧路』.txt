大概是因為伽羅亞瑪利亞的要塞是由石頭堆起來的緣故吧，感覺空氣很冷。

雖然只是單純地呆在房間裡，但冷氣還是刺痛著臉頰。將冰冷的空氣充分吸入肺中，然後變成大大的嘆息向空中吐出。空氣中浮現出白色的霧，然後慢慢地消失了。

不行。已經差不多到極限了。繼續呆在這樣的房間裡是不可能的。
至少，為了暖和身體喝點啤酒也好，但是，放在桌子上的陶器，即使倒過來也不會有一滴酒流下來。我要在這只有冷空氣的房間裡待到什麼時候啊？

最開始，提出這個奇怪提案的是聖女瑪蒂婭的助手拉爾格・安。

──「英雄殿下，您能暫時呆在房間裡嗎？」

安說，那是為紋章教這個組織的紀律著想。

在攻陷傭兵都市貝爾菲因的時候，我沒和任何人說就從伽羅亞瑪利亞離開了，那好像引起了很大的騷動。當然，我並不知道具體情況，但聽說就連那位瑪蒂婭的表情都變了。不過在貝爾菲因的時候沒太能看出來。

安說，萬一有人想做同樣的事就麻煩了。
只要功勞高就可以為所欲為。如果變成那樣的話，組織自然就無法成立了，遲早會從內部被顛覆掉。那對於不太屬於什麼組織的我來說，是不太熟悉的感覺。

因此，即使只是形式上也希望能採取謹慎的形式。而我，現在並沒有什麼非做不可的事情。不過是呆在屋子裡罷了，雖然很狹窄，但也沒什麼大問題，所以我輕輕地點了點頭。現在覺得那真是個不明智的選擇。

沒想到連一杯酒都搞不到，當然，出於謹慎的名義，不能輕易地給予酒也是正常的吧。儘管如此，至少希望你能允許我抽煙啊⋯

肩膀垂了下來，吐了口氣，白色的煙霧又在空中浮現出。乾脆就睡覺去吧，那樣的話時間會過得更快。正在這樣想著的時候，我突然聽到了敲門的聲音。

「英雄殿下，可以打擾一下嗎？」

那個聲音傳到了室內。僅憑這種稱呼，就能推測出站在門前的人是誰。

縱觀這廣闊的世界，只有一個人把我當成英雄。

「啊，當然。如果可以的話，把紅酒或者啤酒作為禮物帶過來是最棒的。」

門咯吱咯吱地響著，在門前的是拉爾格・安。雖然聲音本身與平時沒有區別，但從黑眼圈看來，身體好像相當勉強了。原來如此，能夠處理政務的人才仍然沒有增加啊。在崇尚知識的紋章教中，能夠穩定組織運作的人才是極少的。

其中，安作為瑪蒂婭的助手，聽說背負著以那嬌小的身軀無法想像的政務。眯起眼睛。有一點討厭的預感在指尖奔跑。那麼，肩負著重任的她，為什麼會來拜訪正處於禁閉狀態的我呢？

解除禁閉，或者確認是否逃脫的話，只需要士兵代勞就可以了吧，那麼，為什麼？「很遺憾，我沒帶酒來，英雄殿下。如果要禮物的話只有一個。」雖然這麼說，但是安手裡什麼都沒有。嘴唇和臉頰不由得扭曲了，眉毛上挑。

用手指摸著下巴，催促安繼續說。

「──大聖教移動了它的指尖。卡拉伊斯特王国，即使是在這嚴寒中，軍隊仍以能感受到熱度的勢頭向前推進。」

心臟因為安吐露的言語而跳動。剛才還埋在冷氣中，感覺難以動彈的血液，突然有了一種甦醒的感覺。

「喂、喂，他們瘋了嗎？偏偏在沒有酒就無法入睡的這個時期？」

對我禁不住喃喃到的言語，「對，這個時期」安做出了答覆。我咬著嘴唇的邊緣，眯起眼睛。
真快，比想像中要快得多。我原以為大聖堂開始整兵的時間，必然是在手上能感受到些許溫暖之後。

寒冷時期，特別是大地化著白妝的時期，進軍是相當困難的，而且對資金的需求很大。為了暖和身體，防寒用具變成必需品，酒的消費也變得比平時大得多。如果把這些都去掉的話，鍛鍊出來的精銳姑且不論，為了戰爭而徵集來的劍奴和傭兵們不可能有士氣。而且，如果被積雪絆住軍隊前進腳步的話，兵糧也會白白浪費掉。

當然，寒冷時期進軍的事例也不是從來沒有過。但是，確實不是適合開戰的時候。

「現在，瑪蒂婭大人為了籌劃對策，正在招集大家。英雄殿下。」
「──知道了，雖然不知道能做什麼，但我也要去，對嗎？」

我理解到，在用來答覆安的言語中，不知不覺地浮現出了急躁的情緒。臟腑有種扭曲的感覺。
但是原來如此，如果是這樣的內容，安親自進行傳令也很好理解了。

大聖堂挺起沉重的腰，準備割斷自己的脖子，聽到這些話，大多數人都無法平靜下來。如果信息傳播出現些問題，肯定會引起無謂的混亂。當然，所謂不能平靜下來，我也一樣。
因為紋章教這股勢力曾徹底敗於大聖堂，從歷史上被除名。

聖女瑪蒂婭也將地下神殿作為自己的棺材斷絶了生命。那是過去發生的事實，是不可避免的結局。
咬緊牙根。剛才還很冷的指尖微微發燙。

不行啊。唯獨只有那個，不行。我不想再走和以前一樣的歷史軌跡。雖然過程不同，但如果紋章教獲得了同樣殘酷的結局。那麼，對於我來說，雖然過程不同，迎接同樣的結局也沒什麼奇怪的。

無論是尊嚴，力量，還是心愛之人都無法得到的事實，即使在旅途的盡頭再一次回到那樣的原點，也沒有什麼奇怪的。啊，唯獨這個，不管發生什麼，我都是謝絶的。我可以被人罵做卑鄙，被小人嘲笑也沒關係。

腰邊掛著寶劍，準備走出房間。就在這時，安的聲音再次震動了耳朵。

「──英雄殿下，不，路易斯大人。您不必出席會議。」

那不是平時在周圍回響的聲音。那樣的低沉的爬行著的聲音，至少是我不曾記得聽過的安的聲音。