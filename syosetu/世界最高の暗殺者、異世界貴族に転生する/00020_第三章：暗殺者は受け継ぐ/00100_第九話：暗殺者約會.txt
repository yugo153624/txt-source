在漫長的王都停留時間結束後，馬車在夜間的路上奔馳著。

和父親輪流當車夫，現在輪到我了。
兩側有迪雅和塔爾特，但從剛才開始迪雅就不高興了。

「對不起。我沒有忘記約會的約定。本來以為能抽出一天左右的時間呢。」
「哼。那麼點事我也腦子裡明白。所以不發牢騷。但是，感情方面是不同的。讓我來鬧彆扭吧。」

為了感謝努力開發殺死魔族的魔法，我們約好在王都約會。
但是，接二連三地收到托瓦哈迪等級無法拒絕的貴族們的邀請，連一天自由時間也騰不出來。

⋯⋯我僅有的一小段時間，也被諾伊斯奪去了。雖然他父親在場很老實，但後來卻要求密會。

沒想到他竟那麼狂妄自大。

「我會補償你的。回來後我要去穆爾特，這次才是真正的約會。」
「穆爾特！上次去的時候，有個地方還沒有轉過去。」

方才的不高興就像騙人一樣，臉上一下子明亮起來。
在舞會上瑪哈報告的事，打算去穆爾特一趟。
把迪雅也帶到那裡去。
也有為迪雅準備的東西。

「但是，沒關係嗎？成為聖騎士的羅格大人在城裡接到命令後就必須馬上前往現場了呢。」
「沒事的。預定住兩天一夜。我不會呆太久。」

聖騎士的職位，是當外敵出現時向當地派遣。
在某種程度上，移動是有限制的。

「呵呵，好期待啊。去哪裡好呢？」
「如果沒有特別希望的話，我會來安排。那裡是我的花園。」

以伊魯古・巴洛爾的身份度過了兩年。
那裡可以說是第二故鄉。

「那麼，拜託了。果然，還是希望你能陪我呢⋯⋯下次也不行的話，我會非常失望的。」
「我會努力不變成那樣。假如，在去穆爾特之前出現魔族的話，到時就在當地約會吧。」
「那真是令人期待啊。我沒怎麼去過其他地方呢。」
「沒有那個必要啊。」

除非特別喜歡，否則貴族是不會踏入其他領地的。
迪雅揉眼睛。看起來很睏。

「累了吧。不要勉強，睡覺吧。」

在聚會場上，或許是因為美貌的緣故，所以被貴族們緊緊地纏上了。
正因為是大貴族維科內的千金，所以很習慣待人接物，但並不是不會疲倦。
而且已經很晚了。

「那我就恭敬不如從命了。晚安。」

就這樣，枕著我的大腿開始睡了。
雖然說要撒嬌，但這太撒嬌了。
但是，不錯啊。
迪雅可愛的睡臉在這麼近的地方能看到。
塔爾特羨慕地看著那樣的迪雅。
迪雅微微睜開眼睛。

「看你總是一副很想要的樣子。如果你想做的話，就請你做吧。這是塔爾特的壞習慣。如果你打算客氣的話，請徹底不要露出來。像這樣期待得到羅格的認可，張開嘴等著美味的東西落下來，從不同的意義上來說太撒嬌了，好厚顏無恥啊。」
「哇，我不是那個意思。」
「希望你能再相信羅格和我一點。羅格不會因為塔爾特的任性而覺得麻煩，我也不會生氣的。你看，如果你想要枕著羅格的膝蓋，就這麼說。」
「⋯⋯那個，真的可以嗎？」
「我不介意。雖然不知道羅格是怎麼想的。」
「這樣啊。」
「拜託過你就會明白的。」

說著相當嚴厲的事，不過，是正確的言論。
這一點也是塔爾特的可愛之處。

「啊，那個，羅格大人，我也可以枕膝嗎？」

塔爾特戰戰兢兢地問道。

「啊，沒關係。取而代之的是，當車夫換班的時候，下次就讓我用塔爾特的膝蓋吧。」
「是！我非常期待。」

一邊這麼說，塔爾特頭放在一邊。迪雅改變著位置以便於讓塔爾特的頭放在上面。
兩人同時膝枕，雖然很沉重，但是感覺有比這個更奇妙的幸福成分。
那麼，打起精神來了。我們趕緊回家吧。


◇

回到托瓦哈迪領，經過一天的休養重新振作之後，來到了穆爾特。

「⋯⋯喂，羅格。雖然很久以前就遠離人類了，但是越來越完美地放棄了人類。從托瓦哈迪到這裡，僅僅兩個小時就到了，到底怎樣才能做到啊。」
「以公主抱的方式，抱著迪雅奔跑」

行李多就用馬車，不然可以跑得更快。
因為【鶴皮袋】還很充裕，所以沒必要在意行李。
所以，就那樣做了。
而且，也有想嘗試得到埃波納的力量後，現在的身體能力。

「羅格大人，已經不行了。」

塔爾特坐在那裡。

「不，只要能跟上我，就足夠了。」

我作為避風跑在最前頭，雖然負擔減輕了很多，儘管如此能跟上來是異常的。
在這個國家能辦到的恐怕不到一百人。

「我也嚇了一跳。一想到絕對不想被羅格大人丟下，就堅持下去了。我馬上去幫您安排住宿。約會請加油。」
「啊，拜託了。」

塔爾特的任務，先去做瑪哈委託的事。
就連瑪哈，也想不到我已經來到這裡了。


◇

然後，穆爾特的約會開始了。

在喜歡的帕蒂斯利享受蛋糕。

「嗯，這個鮮奶油的口感最棒了。」
「這裡什麼時候都可以來。」

這裡雖然是很貴的店，但不是高級店。但是，使用的材料質量不亞於高級店⋯

最重要的是，糕點師手藝高超。
鮮奶油和海綿蛋糕。所有的基礎都很突出。
不光是氣氛好的高級店，這樣的實力派的店也是很貴重的。
喝著和蛋糕一起點的香草茶。

「這個是羅格的最愛。」
「看來這裡也是歐露娜的老主顧了。」

化妝品品牌，歐露娜不僅出售化妝品，而且主要以富裕階層女性為目標的香草茶和點心也開始出售。

這個茶葉，因為是購買海外的茶田，使用了在那裡培育的茶葉進行品種改良，所以除了歐露娜以外都買不到。

這麼說來，瑪哈也是這家店的粉絲吧。
不是以這家商店提供的價格出售的茶葉。
是兼作宣傳，廉價批發的吧。是隱藏的名店，顧客層不錯。
和最好的蛋糕一起喝的話，香草茶會更耀眼。
在家中也有很多客人想喝這種茶，是很好的宣傳。瑪哈幹得不錯。

「得給她買點禮物。果然還是有罪惡感⋯⋯下次和塔爾特兩個人單獨約會吧。」
「特產在回去的時候會準備好，所以不用擔心。但是，女方說要和其他孩子約會真是出乎意料。」
「我一般不說。塔爾特因為是個好孩子，無論如何都會在意的。」

不管怎麼說，迪雅把塔爾特當做是重要的朋友。
前幾天在馬車上的台詞也是為塔爾特說的。

「⋯⋯還有，用不愉快的說法或許還有餘地。因為認為自己對羅格來說是最重要的，如果不是這樣，我想大概是嫉妒了。」
「這樣啊。好好地傳達了心情比什麼都好。好了，我們走吧。到下一家商店去。約會才剛剛開始。」
「嗯，走吧。」

牽著手走出店門。
期待今天約會的不只是迪雅，我也是。


◇

迪雅興奮的看著我。

「好厲害啊。明明沒有使用魔法，卻凈是些比魔法更不可思議的事情，即使變成兩半也會活著，人也會瞬間移動。中途開始懷疑是不是在使用魔法，雖然感覺到了魔力，但是真的完全不使用魔法！」

我們看到的就是所謂的魔術表演。
像戲劇之類東西，大貴族的迪雅看膩了，看了最近從海外流行起來的魔術表演。
迪雅比想像中還要中意。

「很有趣吧。」
「羅格你知道是怎麼做的嗎？」
「今天的一切都明白了。」
「騙人。那你說說看。」
「首先，使用人體切斷的床上有裝置，那是床腳上蓋著布的死角吧。就是這樣。其實有兩個人，這樣彎著腰，上半身擔當隱藏下半身，下半身擔當的上半身藏在床下。因為刀刃是通過兩人之間的，所以並沒有被切斷。」
「啊，這麼一說。」

非常單純，因此很難注意到。

「那瞬間移動呢？」
「那是雙胞胎啊。舞台上有一扇隱蔽的門，可以一個人先躲起來。把卡片盡情地放到空中讓布收集起來了吧。觀眾的意識集中在卡片和布上。躲進那個間隙，從雙胞胎離開了的地方有的隱藏的門顯現。隱藏門的前方有個隱藏通道，一般是移動後出現的，但是通過發揮雙胞胎的優點，一瞬間就出現增加了衝擊力。」
「你是怎麼知道是雙胞胎的？」
「仔細看的話，就算是雙胞胎也有不同。不僅僅是臉，連身上穿的東西都是。衣服就是一個。皮革的光澤、污垢、接縫，全部不同。」
「⋯⋯魔力、魔法、身體能力之類的，或者全部超過了那樣的羅格也不是人類吧？」

總覺得對我說了很失禮的話。

「知道這種暢快了嗎？」
「嗯，很暢快。但是，你看破了。」
「這種是習慣。魔術是極端的，可以歸納為兩種技術。一個，製作死角做工。第二，展示想要看的東西，不讓別人看到不想看到的東西。這是我的本職工作。所以，無論如何都能感覺到對方是怎樣引導我們認知的，反射性地看那個反面的東西。不那樣做的話，本職工作就死定了。所以，當你看到對方不想讓你看到的東西時，就會發現它的意圖。」

不僅是基本思想，暗殺的手法中也有很多像魔術一樣的東西。
正因為有這種關係，所以前世也學到了不少戲法。構思、認知操作技術、手巧的鍛鍊。
暗殺和魔術非常合拍。

「真是什麼都會啊。⋯⋯難道，會做出比這個更厲害的魔術嗎？」
「當然可以。」
「那下次讓我看看吧！在宅邸開派對吧。像貴族一樣，不要和各種各樣的家搭話，而是一家人一起熱鬧起來的感覺。所以羅格會很厲害的魔術嗎？」
「看起來很有趣。到時候會協助我嗎？我需要助手。」
「呃，如果不是兩半或看起來像很痛的話。」
「啊，準備了這樣的裝置。如果能當助手的話，我會很安心的，因為助手越是漂亮，魔術就越顯眼。」
「你這麼一說，我就有點害羞了。」

迪將手臂緊緊地纏繞在我的手臂。
兩個人走在夜幕降臨的街道上。

今天的預定結束了，只剩下回旅館了。
過了一會兒，迪雅停了下來。

「怎麼了？」
「喂，羅格，要不要繞道走？」

停住的地方在旅館的前面。
但是，那不是普通的旅館，只是可以稍微休息的地方。
像托瓦哈迪這樣的鄉村，雖然沒有這樣的東西，但是這裡是一個小城市。

「那個，果然還是那個塔爾特啊⋯⋯如果是和母親一起住的宅邸，那種事情有點害羞，所以沒能拜託羅格，但是，這裡的話。」

臉蛋紅得可憐，嘰嘰喳喳地說。
也許是我的錯覺，但有比平時更香甜的氣味。

「沒關係，但我也進入這種地方的話，我也沒有自信能忍耐了。你確定嗎？」
「⋯⋯別問那種事。本來就害羞得要死。」

正如他所說的那樣，他好像已經看不到我的臉了，低著頭。
很久以前，就想和迪雅相愛。

但是，如果伸出手，理智的標誌就會脫落，無論到哪裡都會忍耐著。
但是，讓他這麼說，即使那樣也不出手的話，那還是男人嗎？

⋯⋯而且，以後可能會發生萬一的事情。

「迪雅，我會盡量溫柔的。」

沒有回答，牽著那隻手。
迪雅雖然一直低著頭，但還是緊緊地握著我的手。

終於。
吞了一口口水。

性交這種東西，無論在前世和這邊都經歷過很多次。
但是，第一次將心愛的人與思念重合在一起。

格外緊張。

⋯⋯這麼緊張還是頭一回。甚至暗殺某大國的總統的時候都毫不在乎。

但是，這種緊張感並不會以暗殺者的技術表現出來。
如果我變得不安的話，迪雅會更害怕的。