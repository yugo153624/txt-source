拉爾格•安，感受著自己的臟腑一邊產生一種扭曲的感覺，一邊給予自己的遲鈍的疼痛。身體變得僵硬，向前邁進的腳非常沉重。一不留神，嘴唇中就會流露出嘆息。

這也是沒辦法的事，嘛，比起這個，現在自己必須趕往會場。沒錯，去紋章教和加薩利亞聯合會議的會場。

既然是為了真正的戰爭做準備，那麼，兩個勢力進行聯合會議就不是那麼輕鬆的事了，和以前紋章教和加薩利亞結成同盟時的會議是不同的。
不管怎麼說，那只是一場禮節性意義很強的會議，並非紋章教和加薩利亞聯合作戰的會議。只是在決定了結盟之後，形式上的會議而已。

但是，這次不同。在即將對著自己斬下的大聖堂這把大劍面前，紋章教和加薩利亞必須合力。
要為那做好準備的話，無論有多少時間都不夠用。既然要出席聯合會議，姑且是化著妝的，但是，眼角的黑眼圈卻怎麼也難以掩蓋。

選定會議出席者，制定精密的作戰方案，收集每日變動的信息，以及相應軍備的準備。很難說已經萬事具備了——一個個的不安定因素是沒有盡頭的。
只是，纏繞在安的心上，成為沉重的鎖鏈鎖住雙腿的，並不是那些事，倒不如說非常歡迎那種程度的事成為議題。

安最害怕的是，有人在議場上談起英雄殿下——路易斯的事。

不管怎麼說，我還沒告訴任何人，加薩利亞之主芬•艾爾蒂絲正在尋求他的交付。是的，就連自己的主人，聖女瑪蒂婭也。
實際上，那並不是安對紋章教有背信棄義或不履行職責的行為。畢竟芬•艾爾蒂絲的話語終究是非正式的，只是在決定召開會議的時候，像閑談一樣隨意說出的話。

並且，安到聯合會議召開為止，一手承擔著與加薩利亞間的協調。關於其中發生的事件，可以根據自己的判斷進行處理。安被瑪蒂婭賦予了那樣的權限。

因此，即使將閑談的一兩件事悄悄地沉入心底，也沒有理由受到責備。
但是，我知道。倒不如說不可能不知道。我理應向聖女瑪蒂婭報告。

雖說只是閑話，但那是同盟國的女王要求交付現在無疑是紋章教重要人物路易斯這樣的大問題。因此，如果說今後兩股勢力想要精誠合作的話，這無疑是必須優先處理掉的事項。
在理解了那個的基礎上，安還是怎麼也不能開口。理由是，聖女瑪蒂婭身上令人討厭的變化。

如果將芬•艾爾蒂絲的要求告知本應與算計得失之外的感情全部拉開距離的聖女，結果會怎樣呢?每當想到這一點時，安的腦海裏就會有一種毛骨悚然的感覺。簡直就像惡魔的指尖觸碰著那裡一樣，而且，也會感受到。

可以看到兩種結局。

第一個——聖女瑪蒂婭的腦海中浮現出與平時一樣的精細打算，毫無遺憾地將路易斯交付給加薩利亞。

安認為紋章教應該採取的道路就是那樣。畢竟，並非是失去了路易斯的存在，而且紋章教和加薩利亞間的同盟會變得更堅固吧。倒不如說通過路易斯，能對加薩利亞的方針稍稍插嘴也說不定。

就算考慮到失去作為重要戰力的卡麗婭和芙拉朵這兩個人的可能性，仍然是有好處的。倒不如說也有不該將不穩定的戰力認作戰力的觀點吧。如果是以前的瑪蒂婭，肯定會做出這個選擇。但是，對現在的安來說，還有一種結局清晰地浮現在了眼前。

聖女瑪蒂婭失去計算和理性，按自己的情感來拒絕芬•艾爾蒂絲的要求。就好像知道路易斯獨自前往貝爾菲因時，表現出的激動的神情。

我想否定，我想不可能會有那種事，想確信自己敬愛的聖女，不可能做出那樣的選擇。
但是，每當安要把芬•艾爾蒂絲的事情說出口的時候，這種可能性總是在腦海中留下傷痕。

如果發生了那樣的事，紋章教和加薩利亞的同盟，會變成怎樣呢？雖說不是決裂，但很難想像會發展成良好狀態。在那種情況下，怎麼與強大的大聖堂軍隊對抗？

與加薩利亞的同盟關係產生裂痕，即紋章教遲早有一天會從這片大地上消失。唯有這一點是不能容忍的。

正因如此，安向瑪蒂婭做思想工作，將路易斯置於禁閉狀態。懇切地希望他能有加入紋章教這一勢力的念頭。即使是芬•艾爾蒂絲，如果路易斯本人不同意的話，就不會對紋章教本身懷恨在心。哪怕有某種裂痕，也應該能以最低限度的影響來平息。

但是，路易斯、英雄這樣的存在並不會按照自己的想法來行動。只是想到那個，安就覺得自己的臟腑深處在抽搐。我明白眼睛正在產生某種熱量。
——啊啊，為什麼那個人會這樣？盡是不可理解的事情。
在安的胸中捲起漩渦的感情，是焦躁，悲憤，還有些許對抗心。

拉爾格•安這名少女，其對於事務的處理能力並不是她主要的才能。她大多數的才能在於說話技巧，說服力，談判能力等，說是都屬於對人的能力也不為過。因此，她在紋章教中獲得了協調角色和交涉角色的地位。

不是像卡麗婭和芙拉朵那樣有突出的什麼才能，也不是像瑪蒂婭那樣擁有率領人的才能。雖說是位於幕後處理事務較多的角色，不過，安理解那樣與自己的性質相符，甚至為那樣的角色感到喜悅。

所謂的調整角色、交涉角色，比任何人都更注重與人交往。處於比誰都更能給人帶來影響的立場。

在受到自己的影響之後，人們按照自己預想的那樣進行行動，而且還是組織起來開始共同行動。這和聖女瑪蒂婭是不同的形式，可以說是組織的牽引和操作。對一位名叫拉爾格•安的少女來說，那是秘密的快樂。

結果，那個人。安的牙齒互相咬合，帶來了微弱的疼痛。

老實說，安不擅長應付路易斯。奔放、任性、喜歡女人，時常會懷疑他到底是不是理應和自己同樣擁有理性的人類。
更重要的事，他總是在做出自己預料之外的事情之後露出一副理所當然的樣子。啊啊，不可能應付的來。
這次，我站在調整角色、交涉角色的立場上，在暴露一切的基礎上，希望他能按照我說的做。

結果，路易斯還是沒有給出答案。
――不甘心，恥辱，真不光彩。
眼眶裡連眼淚都快流出來了。啊啊，如果可以的話，真想把那種人盡早交到加薩利亞去。

這次，安還沒有解除路易斯的禁閉。既然他待在那裡，就不會在聯合會議中露面了。之後，為了不讓話題向那邊發展，只需要進行引導…

拖著沉重的腳步，安總算重新振作了精神。進入會場的時候，做了兩次深呼吸。沒事的，自己能做到的。安感覺到自己的心漸漸恢復了平靜。

——啊啦，快過來，路易斯。不來接我的話作為騎士是不合格的吧？

直到聽到那聽起來很快樂似的挑逗著耳朵的聲音。