尼維說的確實沒錯，之前我們討伐了吸血鬼，救下那些人之後，並沒有仔細進行確認，如果裡面混有吸血鬼的話，說不定會發生毀滅性的災難

確認他們有沒有變成吸血鬼的眷屬是非常重要的事情
雖然很重要......但一碼歸一碼
「......已經可以了吧，把那個停下來......」
我看著莉娜的方向對尼維說到，她露出才想起來一樣的神色
「對哦，不小心忘記了。對不起啦」
說著舉起手掌，做出握住什麼東西的動作，包裹著莉娜的蒼白火焰漸漸縮小，最後，啪的一聲消失了
這個聲音似乎莉娜也聽得到
「什，什麼？剛才是什麼！？」
雖然有些驚慌，但看起來並沒有受到傷害，沒關係吧
「......話說回來，為什麼會懷疑莉娜呢？」
羅蕾露這樣問到，尼維回答
「那當然是因為和雷特先生在一起......我開玩笑的，請不要擺出這麼嚇人的表情。其實是因為，我聽說雷特先生從這次誕生的迷宮出來的時候，救出了莉娜小姐，也就是說莉娜小姐之前也被吸血鬼抓住了呢。那麼，因為職業的關係，不把這些疑點燒掉的話......」
說耳朵靈好呢，還是情報收集能力強好呢
雖然說不定只是偶然聽到而已，但就算真的如此，也不想和她扯上什麼關係

我們從那間民宅出來的時候，大概有市民或者冒險者看到了吧
進去的時候是四個人，出來的時候是五個人，很簡單就能明白

話雖如此，一般來說也只會有些在意吧，但對尼維來說，就會產生「那個是吸血鬼嗎」的懷疑。因為她就是那種人啊
能不能思維正常點啊......雖然像這樣說，但她的猜測還真是正中要害，我也沒辦法加以指責

硬要說的話，不是多出來的一人，一起進來的四個人中，其實有兩個都是貨真價實的吸血鬼吶，真遺憾呢，但這種話我是不會說出來的

絕對不說
說出來的話尼維一定會不顧一切殺過去吧......勞拉和伊薩克那邊
不知道哪邊能贏，真是可怕

好奇怪啊......明明我也變強了，怎麼到處都是比我強得多的傢伙呢
羅蕾露也是一樣，雖然身體能力比不上我，但考慮到使用魔術的攻擊力與時常張開的魔力盾，就算進行戰鬥，感覺我也會一敗塗地
我的冒險者之路似乎還難以說是一馬平川啊


「......嘛，至少，現在莉娜的現疑已經消除了吧？」
我對尼維提醒道
在這裡明確一下的話，也能避免今後節外生枝
尼維聽了我的話，回答
「哎，那是自然，已經沒問題了。對不起，莉娜小姐。想要什麼物質賠償麼，之前雷特先生那次，我賠了白金幣二十枚哦」
「唉，白金幣，二十枚......？那麼多錢......唉？」
莉娜看著我的眼神就像在看什麼強盜或者欺詐師一樣
......不不，那又不是我主動要求的
我辯解道
「莉娜，你好像有什麼誤會，我可沒要求那麼多錢。是尼維自己不斷往上加價的」
我才不是那種見錢眼開的人
而且，之前那次姑且也是有著出售塔拉斯孔素材的名義，並非單純索要賠償
雖然金額確實非常巨大就是了
「那麼莉娜小姐有什麼要求嗎？」
尼維這樣問到，莉娜來回搖著頭
「沒，沒有。我......話說回來，你做了什麼需要道歉的事情麼？」
對哦
還沒為她說明
這件事應該是尼維的責任吧，所以我轉向她的方向
......這傢伙還真是毫無意義的非常漂亮啊
生得這副沒人樣子有什麼用麼？
真是令人費解
尼維敗給了我的視線，嘆了口氣
「......一言以蔽之，剛才我懷疑莉娜小姐是吸血鬼，並且出手進行了測試」
「那個......聽剛才的對話，雖然能猜到一點，但具體是怎麼回事呢？我也沒有被做了什麼的感覺......」
那也是理所當然的
無法看到聖氣，也沒有受到傷害，自然察覺不到
尼維回答
「坦白的說，剛剛稍微把你點燃了一下」
說著向我放出了「聖炎」
速度並不很快，感覺想躲的話應該能躲開。難道是顧及到我剛才我讓她不要突然出手麼
才會故意用這種能躲開的速度出招
但直接放出「聖炎」什麼的，還是太隨意了吧
嘛，反正對我也沒有作用
「聖炎」碰到我的身體，然後蔓延開來，將我包裹在一片青白色的火光中

莉娜見狀
「燒，燒燒燒燒起來啦！水！快拿水——！」
手忙腳亂地喊叫起來，我叫住她
「不，一點也不燙，沒關係的。剛才，莉娜差不多就是這種感覺」
「唉？唉——？」
尼維對混亂中的莉娜說到
「這是聖氣產生的特殊火焰，普通人就算碰到，也不會被燙到。但若是吸血鬼的話，就會被燒傷。雖然除非很低級，一般不會直接死掉......嘛，總而言之，用這種火焰燒一下的話，是不是吸血鬼就一目瞭然了」
「一目瞭然......那個......」
莉娜似乎想說點什麼

我能理解她的心情
因為差不多就是吸血鬼嘛
而且我也和她一樣吶
但這種事是不能說出去的，所以莉娜只發出了含糊的聲音
「是、是這樣啊——......」
感覺完全是棒讀啊
再多些演技好嗎


------------
關於莉娜先前看不到聖炎，後來又看得到，其實還是有些小bug
摘錄一段作者的問答


Q：リナも音だけだったり見えたり見えなかったり、炙られてから気がついたようなら聖炎も聖気使いになる何らかの要因って事ですか？
A：普段は聖気持ちじゃないのでリナは見えてない感じですかね。


Q：莉娜一開始只聽聲音，看不見火焰，被燒了之後卻看到了聖炎，和成為了聖氣使有什麼關係麼
A：因為莉娜平時沒有聖氣，所以看不見，這種感覺


考慮到尼維的行動，應該是確定莉娜被聖炎燒過之後就能看到聖氣了
但之前與雷特初次見面時，尼維也曾先後點燃商人沙魯與雷特，沙魯被燒後卻並不能看到聖炎
嘛，可能作者寫的時候也沒想那麼多吧